package com.certant.vtv.controller;

import com.certant.vtv.dto.InspectorRequestDto;
import com.certant.vtv.entity.Inspector;
import com.certant.vtv.service.Datos;
import com.certant.vtv.service.InspectorService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.mockito.Mockito.*;
import static org.hamcrest.Matchers.hasSize;

@Slf4j
@WebMvcTest(InspectorController.class)
class InspectorControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private InspectorService inspectorService;

    ObjectMapper objectMapper;

    @BeforeEach
    void setUp(){
        objectMapper = new ObjectMapper();
    }

    @Test
    void testListaInspectoresController() throws Exception {
        when(inspectorService.getInspectores()).thenReturn(Datos.INSPECTOR_RESPONSE_DTOS);

        mvc.perform(get("/api/v1/inspectores").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].nombre").value("Enzo"))
                .andExpect(jsonPath("$[1].nombre").value("Martin"))
                .andExpect(jsonPath("$[0].apellido").value("Perez"))
                .andExpect(jsonPath("$[1].apellido").value("Fernandez"))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(content().json(objectMapper.writeValueAsString(Datos.INSPECTOR_RESPONSE_DTOS)));
    }

    @Test
    void testAgregarInspectorController() throws Exception {
        InspectorRequestDto inspectorRequestDto = new InspectorRequestDto();
        inspectorRequestDto.setNombre("Juan");
        inspectorRequestDto.setApellido("Castro");
        inspectorRequestDto.setDni("35456595");
        inspectorRequestDto.setLegajo("12346");

        mvc.perform(post("/api/v1/inspector")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(inspectorRequestDto)))
        .andExpect(status().isCreated());
    }

    @Test
    void testAgregarInspectorControllerCampoNulo() throws Exception {
        InspectorRequestDto inspectorRequestDto = new InspectorRequestDto();
        inspectorRequestDto.setNombre("Juan");
        inspectorRequestDto.setApellido("");
        inspectorRequestDto.setDni("35456595");
        inspectorRequestDto.setLegajo("12346");

        mvc.perform(post("/api/v1/inspector")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(inspectorRequestDto)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void testEditarInspectorController() throws Exception {
        when(inspectorService.getInspectorById(1)).thenReturn(Datos.inspector);

        InspectorRequestDto inspectorRequestDto = new InspectorRequestDto();
        inspectorRequestDto.setNombre("Miguel");
        inspectorRequestDto.setApellido("Castellano");
        inspectorRequestDto.setDni("26546521");
        inspectorRequestDto.setLegajo("12350");

        mvc.perform(post("/api/v1/inspector/edit?id=1").contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(inspectorRequestDto)))
                .andExpect(status().isOk());
    }

    @Test
    void testInactivarInspectorController() throws Exception {
        when(inspectorService.getInspectorById(1)).thenReturn(Datos.inspector);
        Inspector inspector = inspectorService.getInspectorById(1);
        mvc.perform(delete("/api/v1/inspector/delete?id=" + inspector.getId()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}