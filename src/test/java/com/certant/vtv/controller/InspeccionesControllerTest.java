package com.certant.vtv.controller;

import com.certant.vtv.constants.Estado;
import com.certant.vtv.dto.InspeccionRequestDto;
import com.certant.vtv.service.InspeccionesService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@WebMvcTest(InspeccionesController.class)
public class InspeccionesControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private InspeccionesService inspeccionesService;

    ObjectMapper objectMapper;

    @BeforeEach
    void setUp(){
        objectMapper = new ObjectMapper();
    }

    private InspeccionRequestDto getInspeccionRequestDto() {
        InspeccionRequestDto inspeccionRequestDto = new InspeccionRequestDto();
        inspeccionRequestDto.setPersonaId(1);
        inspeccionRequestDto.setExento(true);
        inspeccionRequestDto.setEstadoObservaciones(Estado.APTO.toString());
        inspeccionRequestDto.setEstadoMediciones(Estado.APTO.toString());
        inspeccionRequestDto.setPatente("TTT564");
        inspeccionRequestDto.setVersionId(1);
        inspeccionRequestDto.setNombre("Jose");
        inspeccionRequestDto.setApellido("Sanchez");
        inspeccionRequestDto.setDni("32553684");
        inspeccionRequestDto.setTelefono("1177445544");
        inspeccionRequestDto.setCorreo("jsanchez@mail.com");
        return inspeccionRequestDto;
    }

    @Test
    void testAgregarInspeccionController() throws Exception {
        InspeccionRequestDto inspeccionRequestDto = getInspeccionRequestDto();
        mvc.perform(post("/api/v1/inspeccion")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(inspeccionRequestDto)))
                .andExpect(status().isCreated());
    }

    @Test
    void testAgregarInspeccionControllePatenteFallida() throws Exception {
        InspeccionRequestDto inspeccionRequestDto = getInspeccionRequestDto();
        inspeccionRequestDto.setPatente("ASEC213");
        mvc.perform(post("/api/v1/inspeccion")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(inspeccionRequestDto)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void testAgregarInspeccionControllerCorreoFallido() throws Exception {
        InspeccionRequestDto inspeccionRequestDto = getInspeccionRequestDto();
        inspeccionRequestDto.setCorreo("correosinformato");
        mvc.perform(post("/api/v1/inspeccion")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(inspeccionRequestDto)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void testAgregarInspeccionControllerTelefonoFallido() throws Exception {
        InspeccionRequestDto inspeccionRequestDto = getInspeccionRequestDto();
        inspeccionRequestDto.setTelefono("1234567");
        mvc.perform(post("/api/v1/inspeccion")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(inspeccionRequestDto)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void testAgregarInspeccionControllerInspectorNulo() throws Exception {
        InspeccionRequestDto inspeccionRequestDto = getInspeccionRequestDto();
        inspeccionRequestDto.setPersonaId(null);
        mvc.perform(post("/api/v1/inspeccion")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(inspeccionRequestDto)))
                .andExpect(status().isBadRequest());
    }

}
