package com.certant.vtv.controller;

import com.certant.vtv.dto.AutomovilRequestDto;
import com.certant.vtv.service.AutomovilService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import java.time.LocalDate;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@WebMvcTest(AutomovilController.class)
public class AutomovilControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private AutomovilService automovilService;

    ObjectMapper objectMapper;

    @BeforeEach
    void setUp(){
        objectMapper = new ObjectMapper();
    }

    private AutomovilRequestDto getAutomovilRequestDto() {
        AutomovilRequestDto automovilRequestDto = new AutomovilRequestDto();
        automovilRequestDto.setVto(LocalDate.now().toString());
        automovilRequestDto.setEstado("APTO");
        automovilRequestDto.setVersionId(1);
        automovilRequestDto.setNombre("Jose");
        automovilRequestDto.setApellido("Sanchez");
        automovilRequestDto.setDni("32553684");
        automovilRequestDto.setTelefono("1177445544");
        automovilRequestDto.setCorreo("jsanchez@mail.com");
        return automovilRequestDto;
    }

    @Test
    void testAgregarAutomovilControllerPartenteFormatoNuevo() throws Exception {
        AutomovilRequestDto automovilRequestDto = getAutomovilRequestDto();
        automovilRequestDto.setPatente("AS122RD");
        mvc.perform(post("/api/v1/automovil")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(automovilRequestDto)))
                .andExpect(status().isCreated());
    }

    @Test
    void testAgregarAutomovilControllerPartenteFormatoViejo() throws Exception {
        AutomovilRequestDto automovilRequestDto = getAutomovilRequestDto();
        automovilRequestDto.setPatente("AST122");
        mvc.perform(post("/api/v1/automovil")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(automovilRequestDto)))
                .andExpect(status().isCreated());
    }

    @Test
    void testAgregarAutomovilControllerPatenteSinFormatoViejo() throws Exception {
        AutomovilRequestDto automovilRequestDto = getAutomovilRequestDto();
        automovilRequestDto.setPatente("AST1231");

        mvc.perform(post("/api/v1/automovil")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(automovilRequestDto)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void testAgregarAutomovilControllerPatenteSinFormatoNuevo() throws Exception {
        AutomovilRequestDto automovilRequestDto = getAutomovilRequestDto();
        automovilRequestDto.setPatente("AS456JOP");

        mvc.perform(post("/api/v1/automovil")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(automovilRequestDto)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void testAgregarAutomovilControllerTelefono7Digitos() throws Exception {
        AutomovilRequestDto automovilRequestDto = getAutomovilRequestDto();
        automovilRequestDto.setPatente("AA111AA");
        automovilRequestDto.setTelefono("1234567");

        mvc.perform(post("/api/v1/automovil")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(automovilRequestDto)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void testAgregarAutomovilControllerTelefono13Digitos() throws Exception {
        AutomovilRequestDto automovilRequestDto = getAutomovilRequestDto();
        automovilRequestDto.setPatente("AA111AA");
        automovilRequestDto.setTelefono("1234567890123");

        mvc.perform(post("/api/v1/automovil")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(automovilRequestDto)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void testAgregarAutomovilControllerTelefonoApto() throws Exception {
        AutomovilRequestDto automovilRequestDto = getAutomovilRequestDto();
        automovilRequestDto.setPatente("AA111AA");
        automovilRequestDto.setTelefono("12345678");

        mvc.perform(post("/api/v1/automovil")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(automovilRequestDto)))
                .andExpect(status().isCreated());
    }
}
