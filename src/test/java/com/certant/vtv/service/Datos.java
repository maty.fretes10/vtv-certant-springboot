package com.certant.vtv.service;

import com.certant.vtv.constants.Estado;
import com.certant.vtv.constants.TipoVehiculos;
import com.certant.vtv.dto.InspeccionResponseDto;
import com.certant.vtv.dto.InspectorResponseDto;
import com.certant.vtv.entity.*;
import org.modelmapper.ModelMapper;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Datos {

    public static Marca marca = new Marca(1, "Chevrolet");
    public static Modelo modelo = new Modelo(1, "Corsa", marca);
    public static Version version = new Version(1, "1.6", modelo);
    public static Inspector inspector = new Inspector(1, "Matias", "Fretes", "42424242", "12345", true);
    public static Inspector inspector2 = new Inspector(2, "Nicolas", "Lopez", "45454545", "12346", true);
    public static Propietario propietario = new Propietario(3, "Juan", "Perez", "35663552", "jperez@mail.com", "1122335566", true);

    public final static List<Inspector> INSPECTOR_LIST = Arrays.asList(inspector, inspector2);

    private final static InspectorResponseDto INSPECTOR_RESPONSE_DTO_01 = new InspectorResponseDto(3, "Enzo", "Perez","36665595", "12348");
    private final static InspectorResponseDto INSPECTOR_RESPONSE_DTO_02 = new InspectorResponseDto(4, "Martin", "Fernandez","32312311", "12349");

    public final static List<InspectorResponseDto> INSPECTOR_RESPONSE_DTOS =Arrays.asList(INSPECTOR_RESPONSE_DTO_01, INSPECTOR_RESPONSE_DTO_02);

    public static Vehiculo vehiculo =
            new Vehiculo(1,
                    "LFR322",
                    LocalDate.now(),
                    TipoVehiculos.AUTOMOVIL,
                    "APTO", version, inspector, true);

    public static Automovil automovil =
            new Automovil(1, "GRW654", LocalDate.now(), "APTO", version, propietario, true);

    public static Inspeccion inspeccion =
            new Inspeccion(1,
                    LocalDate.now(),
                    Estado.APTO,
                    Estado.APTO,
                    Estado.APTO,
                    false,
                    vehiculo,
                    inspector,
                    true);


    public final static List<Inspeccion> INSPECCION_LIST = Arrays.asList(inspeccion);

    public final static List<Automovil> AUTOMOVIL_LIST = Arrays.asList(automovil);
}
