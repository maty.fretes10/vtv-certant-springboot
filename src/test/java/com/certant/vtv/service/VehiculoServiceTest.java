package com.certant.vtv.service;

import com.certant.vtv.entity.Automovil;
import com.certant.vtv.exception.ResourceNotFoundException;
import com.certant.vtv.repository.AutomovilRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@Slf4j
@ExtendWith(MockitoExtension.class)
public class VehiculoServiceTest {

    @Mock
    AutomovilRepository repository;
    @Mock
    ModelMapper modelMapper;
    @Mock
    VersionService versionService;
    @Mock
    PropietarioService propietarioService;
    @Mock
    VehiculoService vehiculoService;

    @InjectMocks
    AutomovilesServiceImpl automovilesService;


    @Test
    void testAutomovilIdNulo(){
        assertThrows(IllegalStateException.class, () -> { automovilesService.getAutomovilId(null); });
    }

    @Test
    void testAutomovilIdInexistente(){
        assertThrows(ResourceNotFoundException.class, () -> {
            automovilesService.getAutomovilId(-1);
        });
    }

    @Test
    void testInactivarDosVeces(){
        when(repository.findById(1)).thenReturn(Optional.ofNullable(Datos.automovil));
        automovilesService.deleteAutomovil(1);
        assertThrows(IllegalArgumentException.class, () ->
                automovilesService.deleteAutomovil(1));
    }

    @Test
    void testPropietarioPorVehiculo(){
        when(repository.findById(1)).thenReturn(Optional.ofNullable(Datos.automovil));
        assertEquals("Juan", automovilesService.getPropietarioVehiculo(1).getNombre());
        assertEquals("35663552", automovilesService.getPropietarioVehiculo(1).getDni());

        //Casos bordes
        assertNotEquals("Jua", automovilesService.getPropietarioVehiculo(1).getNombre());
        assertNotEquals("356635521", automovilesService.getPropietarioVehiculo(1).getDni());

        //Controlar que findById del repository se invoco dos veces.
        verify(repository, times(4)).findById(1);
    }

    @Test
    void testVehiculoPorPatente(){
        when(repository.findById(1)).thenReturn(Optional.ofNullable(Datos.automovil));
        when(repository.findAll()).thenReturn(Datos.AUTOMOVIL_LIST);
        Automovil auto = automovilesService.getAutomovilById(1);
        log.info(auto.getPatente());
        Automovil automovil = automovilesService.automovilPorPatente("GRW654");
        Automovil automovil2 = automovilesService.automovilPorPatente("GRW655");

        assertEquals("GRW654", automovil.getPatente());
        assertNull(automovil2);
    }
}
