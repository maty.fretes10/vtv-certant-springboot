package com.certant.vtv.service;

import com.certant.vtv.entity.Inspector;
import com.certant.vtv.exception.ResourceNotFoundException;
import com.certant.vtv.repository.InspectorRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import java.util.Optional;
import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@ExtendWith(MockitoExtension.class)
class InspectorServiceTest {

    @Mock
    InspectorRepository repository;
    @Mock
    ModelMapper modelMapper;

    @InjectMocks
    InspectorServiceImpl inspectorService;

    @BeforeEach
    void setUp(){
    }

    @Test
    void testListaInspectores() {
        when(repository.findAll()).thenReturn(Datos.INSPECTOR_LIST);
        assertNotNull(inspectorService.getInspectores());
    }

    @Test
    void testInspectorPresente(){
        when(repository.findById(1)).thenReturn(Optional.ofNullable(Datos.INSPECTOR_LIST.get(0)));
        Inspector inspector = inspectorService.getInspectorById(1);
        assertEquals("Matias", inspector.getNombre());
    }

    @Test
    void testInspectorIdNulo(){
       assertThrows(IllegalStateException.class, () -> { inspectorService.getInspectorById(null); });
    }

    @Test
    void testInspectorIdInexistente(){
        assertThrows(ResourceNotFoundException.class, () -> {
                    inspectorService.getInspectorById(-1);
                });
    }

    @Test
    void testExistsLegajo(){
        when(repository.findAll()).thenReturn(Datos.INSPECTOR_LIST);
        assertTrue(inspectorService.existsLegajo("12346"));
    }

    @Test
    void testInactivarDosVeces(){
        when(repository.findById(2)).thenReturn(Optional.ofNullable(Datos.inspector2));
        inspectorService.deleteInspector(2);
        assertThrows(IllegalArgumentException.class, () ->
                inspectorService.deleteInspector(2));
    }
}