package com.certant.vtv.service;

import com.certant.vtv.constants.Estado;
import com.certant.vtv.exception.ResourceNotFoundException;
import com.certant.vtv.repository.InspeccionRepository;
import com.certant.vtv.repository.PropietarioRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import java.time.LocalDate;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@Slf4j
@ExtendWith(MockitoExtension.class)
class InspeccionesServiceTest {

    @Mock
    InspeccionRepository repository;
    @Mock
    ModelMapper modelMapper;
    @Mock
    AutomovilService automovilService;
    @Mock
    InspectorService inspectorService;
    @Mock
    PropietarioRepository propietarioRepository;

    @InjectMocks
    InspeccionesServiceImpl inspeccionesService;

    @Test
    void testListaInspecciones() {
        when(repository.findAll()).thenReturn(Datos.INSPECCION_LIST);
        assertNotNull(inspeccionesService.getInspecciones());
    }

    @Test
    void testDecidirVencimiento(){
        assertEquals(LocalDate.now().plusYears(1).toString(), inspeccionesService.decidirVencimiento(Estado.APTO));
        assertNotEquals(LocalDate.now().toString(), inspeccionesService.decidirVencimiento(Estado.APTO));
        assertEquals(LocalDate.now().toString(), inspeccionesService.decidirVencimiento(Estado.CONDICIONAL));
        assertEquals(LocalDate.now().toString(), inspeccionesService.decidirVencimiento(Estado.RECHAZADO));
    }

    @Test
    void testEstadoFinalRechazado(){
        assertEquals(Estado.RECHAZADO, inspeccionesService.decidirEstadoFinal(Estado.RECHAZADO, Estado.RECHAZADO));
        assertEquals(Estado.RECHAZADO, inspeccionesService.decidirEstadoFinal(Estado.RECHAZADO, Estado.APTO));
        assertEquals(Estado.RECHAZADO, inspeccionesService.decidirEstadoFinal(Estado.RECHAZADO, Estado.CONDICIONAL));
        assertEquals(Estado.RECHAZADO, inspeccionesService.decidirEstadoFinal(Estado.APTO, Estado.RECHAZADO));
        assertEquals(Estado.RECHAZADO, inspeccionesService.decidirEstadoFinal(Estado.CONDICIONAL, Estado.RECHAZADO));
    }

    @Test
    void testEstadoFinalCondicional(){
        assertEquals(Estado.CONDICIONAL, inspeccionesService.decidirEstadoFinal(Estado.CONDICIONAL, Estado.CONDICIONAL));
        assertEquals(Estado.CONDICIONAL, inspeccionesService.decidirEstadoFinal(Estado.CONDICIONAL, Estado.APTO));
        assertEquals(Estado.CONDICIONAL, inspeccionesService.decidirEstadoFinal(Estado.APTO, Estado.CONDICIONAL));
    }

    @Test
    void testEstadoFinalApto(){
        assertEquals(Estado.APTO, inspeccionesService.decidirEstadoFinal(Estado.APTO, Estado.APTO));
    }

    /*@Test
    void testArgumentMatchers(){
        when(repository.findAll()).thenReturn(Inspecciones.INSPECCION_LIST);
        when(repository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(Inspecciones.INSPECCION_LIST.get(Mockito.anyInt())));
        List<InspeccionResponseDto> inspeccion = inspeccionesService.getInspeccionesByPropietario("Matias");
        log.info(inspeccion.toString());

        verify(repository).findAll();
        verify(repository).findById(argThat(arg -> arg.equals(1)));

    }*/

    @Test
    void testInspeccionIdNulo(){
        assertThrows(IllegalStateException.class, () -> { inspeccionesService.getInspeccionById(null); });
    }

    @Test
    void testInspeccionIdInexistente(){
        assertThrows(ResourceNotFoundException.class, () -> {
            inspeccionesService.getInspeccionById(-1);
        });
    }

    @Test
    void testInactivarDosVeces(){
        when(repository.findById(1)).thenReturn(Optional.ofNullable(Datos.inspeccion));
        inspeccionesService.inactivarInspeccion(1);
        assertThrows(IllegalArgumentException.class, () ->
                inspeccionesService.inactivarInspeccion(1));
    }

}