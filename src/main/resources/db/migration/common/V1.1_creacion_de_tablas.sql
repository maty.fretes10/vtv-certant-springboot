CREATE TABLE `vtvcertant`.`persona` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `apellido` VARCHAR(45) NOT NULL,
  `tipo` VARCHAR(45) NOT NULL,
  `dni` VARCHAR(45) NOT NULL UNIQUE,
  `activo` BOOLEAN NOT NULL,
  PRIMARY KEY (`id`));

  CREATE TABLE `vtvcertant`.`inspector` (
  `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `ins_legajo` VARCHAR(45) NOT NULL UNIQUE,
  CONSTRAINT fk_inspector foreign key (id) references persona(id));


CREATE TABLE `vtvcertant`.`propietario` (
  `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `telefono` VARCHAR(45) NOT NULL,
  `correo` VARCHAR(45) NOT NULL,
  CONSTRAINT fk_propietario foreign key (id) references persona(id));

CREATE TABLE `vtvcertant`.`marca` (
	`id`  INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `descripcion` VARCHAR(45) NOT NULL
);

CREATE TABLE `vtvcertant`.`modelo` (
	`id`  INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `descripcion` VARCHAR(45) NOT NULL,
    `marca_id` INTEGER NOT NULL,
    CONSTRAINT fk_modelo_marca foreign key (marca_id) references marca(id)
);

CREATE TABLE `vtvcertant`.`version` (
	`id` INT NOT NULL auto_increment PRIMARY KEY,
	`descripcion` VARCHAR(45) NOT NULL,
	`modelo_id` INTEGER NOT NULL,
    CONSTRAINT fk_version_modelo foreign key (modelo_id) references modelo(id));

CREATE TABLE `vtvcertant`.`vehiculo` (
	`id`  INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `patente` VARCHAR(45) NOT NULL UNIQUE,
	`vto` DATE NOT NULL,
	`tipo` VARCHAR(45) NOT NULL,
    `estado` VARCHAR(45) NOT NULL,
    `activo` BOOLEAN NOT NULL,
    `id_persona` INT NOT NULL,
	`id_version` INT NOT NULL,
    CONSTRAINT fk_vehiculo_persona foreign key (id_persona) references persona(id),
    CONSTRAINT fk_vehiculo_version foreign key (id_version) references `vtvcertant`.`version`(id)
);

CREATE TABLE `vtvcertant`.`automovil` (
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	CONSTRAINT fk_automovil_vehiculo foreign key (id) references vehiculo(id)
);


CREATE TABLE `vtvcertant`.`inspeccion` (
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `fecha` DATE NOT NULL,
    `estado_observaciones` VARCHAR(45) NOT NULL,
	`estado_mediciones` VARCHAR(45) NOT NULL,
	`estado_final` VARCHAR(45) NOT NULL,
    `exento` boolean NOT NULL,
    `activo` BOOLEAN NOT NULL,
    `id_vehiculo` INT NOT NULL,
    `id_persona` INT NOT NULL,
	CONSTRAINT fk_vehiculo_inspeccion foreign key (id_vehiculo) references vehiculo(id),
    CONSTRAINT fk_inspector_inspeccion foreign key (id_persona) references persona(id)
);

