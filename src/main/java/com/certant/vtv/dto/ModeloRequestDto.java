package com.certant.vtv.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ModeloRequestDto implements Serializable {
    @NotNull(message = "Por favor ingrese el id de la marca")
    private Integer marcaId;
    @NotEmpty(message = "Por favor ingrese el nombre del modelo")
    private String descripcion;
}
