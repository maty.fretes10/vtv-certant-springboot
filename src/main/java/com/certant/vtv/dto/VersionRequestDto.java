package com.certant.vtv.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class VersionRequestDto implements Serializable {
    @NotNull(message = "Por favor ingrese el id del modelo")
    private Integer modeloId;
    @NotEmpty(message = "Por favor ingrese el nombre de la version")
    private String descripcion;
}
