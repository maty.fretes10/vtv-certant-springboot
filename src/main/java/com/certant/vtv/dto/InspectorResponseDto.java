package com.certant.vtv.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class InspectorResponseDto {
    private Integer id;
    private String nombre;
    private String apellido;
    private String dni;
    private String legajo;
}
