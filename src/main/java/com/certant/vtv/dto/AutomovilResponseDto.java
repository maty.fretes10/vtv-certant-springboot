package com.certant.vtv.dto;

import com.certant.vtv.entity.Persona;
import com.certant.vtv.entity.Version;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class AutomovilResponseDto {
    private Integer id;
    private String patente;
    private String vto;
    private String estado;
    private VersionResponseDto version;
    private Persona persona;
}
