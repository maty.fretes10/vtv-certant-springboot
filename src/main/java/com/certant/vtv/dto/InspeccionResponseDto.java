package com.certant.vtv.dto;

import com.certant.vtv.constants.Estado;
import com.certant.vtv.entity.Persona;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class InspeccionResponseDto {
    private Integer id;
    private LocalDate fecha;
    private Estado estadoObservaciones;
    private Estado estadoMediciones;
    private Estado estadoFinal;
    private boolean exento;
    private VehiculoResponseDto vehiculo;
    private Persona persona;
}
