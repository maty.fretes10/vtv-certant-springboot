package com.certant.vtv.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class AutomovilRequestDto {

    @NotEmpty(message = "Por favor ingrese la patente")
    @Pattern(regexp = "^([a-zA-Z]{2,3}[0-9]{3}[a-zA-Z]{0,2})?$", message = "Formato de patente no valido - Formatos validos: AAA111 o AA111AA")
    private String patente;
    @NotEmpty(message = "Por favor ingrese el vencimiento")
    private String vto;
    @NotEmpty(message = "Por favor ingrese el estado")
    private String estado;
    @NotNull(message = "Por favor ingrese el id de la version")
    private Integer versionId;
    @NotEmpty(message = "Por favor ingrese un nombre")
    private String nombre;
    @NotEmpty(message = "Por favor ingrese un apellido")
    private String apellido;
    @NotEmpty(message = "Por favor ingrese un dni")
    private String dni;
    @NotEmpty(message = "Por favor ingrese un telefono")
    @Size(min = 8, max = 12, message="El  telefono debe tener entre 8 - 12 digitos")
    private String telefono;
    @NotEmpty(message = "Por favor ingrese un correo")
    private String correo;
}
