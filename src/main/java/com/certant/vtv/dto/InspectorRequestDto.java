package com.certant.vtv.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class InspectorRequestDto {
    @NotEmpty(message = "Por favor ingrese un nombre")
    private String nombre;
    @NotEmpty(message = "Por favor ingrese un apellido")
    private String apellido;
    @NotEmpty(message = "Por favor ingrese un dni")
    private String dni;
    @NotEmpty(message = "Por favor ingrese un legajo")
    private String legajo;
}
