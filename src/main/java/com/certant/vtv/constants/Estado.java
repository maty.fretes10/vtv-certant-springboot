package com.certant.vtv.constants;

public enum Estado {
    APTO, CONDICIONAL, RECHAZADO
}
