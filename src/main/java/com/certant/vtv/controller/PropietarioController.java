package com.certant.vtv.controller;

import com.certant.vtv.dto.PropietarioRequestDto;
import com.certant.vtv.dto.PropietarioResponseDto;
import com.certant.vtv.service.PropietarioService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1")

public class PropietarioController {
	private final PropietarioService propietarioService;

	@Autowired
	public PropietarioController(PropietarioService propietarioService) {
		this.propietarioService = propietarioService;
	}
	
	@GetMapping("/propietarios")
	public List<PropietarioResponseDto> getPropietarios() throws Exception{
		return propietarioService.getPropietarios();
	}
	
	@PostMapping("/propietario")
	@ResponseStatus(HttpStatus.CREATED)
	public void addPropietario(@Valid @RequestBody PropietarioRequestDto dto) {
		propietarioService.addPropietario(dto);
	}

	@PostMapping("/propietario/edit")
	public void editPropietario(@RequestParam Integer id, @Valid @RequestBody PropietarioRequestDto requestDto){
		propietarioService.editPropietario(id, requestDto);
	}

	@DeleteMapping("/propietario/delete")
	public void deletePropietario(@RequestParam Integer id){
		propietarioService.inactivarPropietario(id);
	}
}
