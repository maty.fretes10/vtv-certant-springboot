package com.certant.vtv.controller;

import com.certant.vtv.dto.InspectorRequestDto;
import com.certant.vtv.dto.InspectorResponseDto;
import com.certant.vtv.service.InspectorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1")

public class InspectorController {
	private final InspectorService inspectorService;

	@Autowired
	public InspectorController(InspectorService inspectorService) {
		this.inspectorService = inspectorService;
	}

	@GetMapping("/inspector")
	public InspectorResponseDto getInspector(@RequestParam Integer id){
		return inspectorService.getInspectorByIdResponse(id);
	}
	
	@GetMapping("/inspectores")
	public List<InspectorResponseDto> getInspectores() throws Exception{
		return inspectorService.getInspectores();
	}
	
	@PostMapping("/inspector")
	@ResponseStatus(HttpStatus.CREATED)
	public void addInspector(@Valid @RequestBody InspectorRequestDto dto) {
		inspectorService.addInspector(dto);
	}

	@PostMapping("/inspector/edit")
	public void editInspector(@RequestParam Integer id, @Valid @RequestBody InspectorRequestDto requestDto){
		inspectorService.editInspector(id, requestDto);
	}

	@DeleteMapping("/inspector/delete")
	public void deleteInspector(@RequestParam Integer id){
		inspectorService.deleteInspector(id);
	}
}
