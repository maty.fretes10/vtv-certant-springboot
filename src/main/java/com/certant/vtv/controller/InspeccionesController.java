package com.certant.vtv.controller;

import com.certant.vtv.dto.InspeccionRequestDto;
import com.certant.vtv.dto.InspeccionResponseDto;
import com.certant.vtv.service.InspeccionesService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@Slf4j
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1")

public class InspeccionesController {
	private final InspeccionesService inspeccionesService;

	@Autowired
	public InspeccionesController(InspeccionesService inspeccionesService) {
		this.inspeccionesService = inspeccionesService;
	}
	
	@PostMapping("/inspeccion")
	@ResponseStatus(HttpStatus.CREATED)
	public void addInspeccion(@Valid @RequestBody InspeccionRequestDto dto) {
		inspeccionesService.addInspecciones(dto);
	}

	@GetMapping("/inspecciones")
	public List<InspeccionResponseDto> getInspecciones(){
		return inspeccionesService.getInspecciones();
	}

	@GetMapping("/inspecciones/propietario")
	public List<InspeccionResponseDto> getInspeccionesByPropietario(@RequestParam(required = false) String palabra){
		return inspeccionesService.getInspeccionesByPropietario(palabra);
	}

	@PostMapping("/inspeccion/edit")
	public void editInspeccion(@RequestParam Integer id, @Valid @RequestBody InspeccionRequestDto requestDto){
		inspeccionesService.editInspeccion(id, requestDto);
	}

	@DeleteMapping("/inspeccion/inactivar")
	public void inactivarInspeccion(@RequestParam Integer id){
		inspeccionesService.inactivarInspeccion(id);
	}
}
