package com.certant.vtv.controller.FrontEnd;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@Slf4j
public class FrontEndController {

    @GetMapping("/")
    public String inicio(){
        return "index";
    }
}
