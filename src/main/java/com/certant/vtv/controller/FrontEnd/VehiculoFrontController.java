package com.certant.vtv.controller.FrontEnd;

import com.certant.vtv.dto.*;
import com.certant.vtv.entity.Automovil;
import com.certant.vtv.entity.Marca;
import com.certant.vtv.entity.Version;
import com.certant.vtv.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@Controller
@RequestMapping("/views/vehiculo")
public class VehiculoFrontController {

    private final AutomovilService automovilService;
    private final MarcaService marcaService;
    private final ModeloService modeloService;
    private final VersionService versionService;
    private final VehiculoService vehiculoService;


    @Autowired
    public VehiculoFrontController(AutomovilService automovilService,
                                   MarcaService marcaService,
                                   ModeloService modeloService,
                                   VersionService versionService,
                                   VehiculoService vehiculoService) {
        this.automovilService = automovilService;
        this.marcaService = marcaService;
        this.modeloService = modeloService;
        this.versionService = versionService;
        this.vehiculoService = vehiculoService;
    }

    @GetMapping("/")
    public String listarVehiculos(Model model, @RequestParam(required = false) String estado){
        List<VehiculoResponseDto> list = vehiculoService.getVehiculosByEstado(estado);
        model.addAttribute("titulo", "Listado de vehiculos");
        model.addAttribute("vehiculos", list);
        model.addAttribute("estado", estado);
        return "/views/vehiculo/listar";
    }

    int idMarca = 0;
    int idVersion = 1;

    @GetMapping("/nuevo")
    public String nuevoVehiculo(Model model){
        List<ModeloResponseDto> modelos = modeloService.getModeloByIdMarca(idMarca);
        AutomovilRequestDto vehiculo = new AutomovilRequestDto();
        Version version = versionService.getVersionById(idVersion);

        model.addAttribute("titulo", "Nuevo vehiculo");
        model.addAttribute("vehiculo", vehiculo);
        model.addAttribute("modelos", modelos);
        model.addAttribute("version", version);

        return "/views/vehiculo/frmCrear";
    }

    @PostMapping("/guardar")
    public String guardar(@Valid @ModelAttribute("vehiculo") AutomovilRequestDto dto,
                          BindingResult result, Model model, RedirectAttributes attributes) {
        Version version = versionService.getVersionById(dto.getVersionId());
        Marca marca = version.getModelo().getMarca();
        List<ModeloResponseDto> modelos = modeloService.getModeloByIdMarca(marca.getId());
        List<VersionResponseDto> versiones = versionService.getVersionesByIdModelo(version.getModelo().getId());
        if(result.hasErrors()){
            log.info(result.toString());
            model.addAttribute("titulo", "Nuevo vehiculo");
            model.addAttribute("vehiculo", dto);
            model.addAttribute("versiones", versiones);
            model.addAttribute("version", version);
            model.addAttribute("modelos", modelos);
            return "/views/vehiculo/frmCrear";
        }

        automovilService.addAutomovil(dto);
        attributes.addFlashAttribute("success" , "Vehiculo registrado con éxito");
        return "redirect:/views/vehiculo/";
    }

    @ModelAttribute("marcas")
    public List<MarcaResponseDto> getMarcas() {
        return marcaService.getMarcas();
    }

    @GetMapping(value = "/buscarPorMarca")
    public @ResponseBody
    List<ModeloResponseDto> todosModelosPorMarca(
            @RequestParam(value = "idMarca", required = true) int idMarca) {
        return modeloService.getModeloByIdMarca(idMarca);
    }

    @GetMapping(value = "/buscarPorModelo")
    public @ResponseBody List<VersionResponseDto> todasVersionesPorModelo(
            @RequestParam(value = "idModelo", required = true) int idModelo) {
        return versionService.getVersionesByIdModelo(idModelo);
    }

    Integer id = 0;

    @GetMapping("/editar/{id}")
    public String editarVehiculos(@PathVariable("id") Integer idVehiculo, Model model){
        Automovil automovil = automovilService.getAutomovilById(idVehiculo);
        Integer idMarcaEdicion = automovil.getVersion().getModelo().getMarca().getId();
        List<ModeloResponseDto> modelos = modeloService.getModeloByIdMarca(idMarcaEdicion);
        Integer idModeloEdicion= automovil.getVersion().getModelo().getId();
        List<VersionResponseDto> versiones = versionService.getVersionesByIdModelo(idModeloEdicion);

        model.addAttribute("titulo", "Editar vehiculos");
        model.addAttribute("modelos", modelos);
        model.addAttribute("versiones", versiones);
        model.addAttribute("vehiculo",automovil);

        id = idVehiculo;
        return "/views/vehiculo/frmEditar";
    }

    @PostMapping("/edicion")
    public String editar(@Valid @ModelAttribute("vehiculo") AutomovilRequestDto dto,
                         BindingResult result, Model model,
                         RedirectAttributes attributes){
        Automovil automovil = automovilService.getAutomovilById(id);
        Integer idMarcaEdicion = automovil.getVersion().getModelo().getMarca().getId();
        List<ModeloResponseDto> modelos = modeloService.getModeloByIdMarca(idMarcaEdicion);
        Integer idModeloEdicion= automovil.getVersion().getModelo().getId();
        List<VersionResponseDto> versiones = versionService.getVersionesByIdModelo(idModeloEdicion);

        if(result.hasErrors()){
            log.info(result.toString());
            model.addAttribute("titulo", "Editar vehiculos");
            model.addAttribute("modelos", modelos);
            model.addAttribute("versiones", versiones);
            model.addAttribute("vehiculo",automovil);
            attributes.addFlashAttribute("error" , "No se realizaron los cambios por: " + result.getFieldError().getDefaultMessage());
            attributes.addFlashAttribute("info" , "Se reestablecieron los valores");
            return "redirect:/views/vehiculo/editar/" + id;
        }

        automovilService.editAutomovil(id, dto);
        attributes.addFlashAttribute("info" , "Vehiculo editado con éxito");
        return "redirect:/views/vehiculo/";
    }

    @GetMapping("/inactivar/{id}")
    public String inactivarInspeccion(@PathVariable("id") Integer idVehiculo,
                                      RedirectAttributes attributes){
        automovilService.deleteAutomovil(idVehiculo);
        attributes.addFlashAttribute("warning" , "Vehiculo inactivado");
        return "redirect:/views/vehiculo/";
    }
}
