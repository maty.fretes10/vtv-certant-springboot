package com.certant.vtv.controller.FrontEnd;

import com.certant.vtv.dto.PropietarioResponseDto;
import com.certant.vtv.service.PropietarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/views/propietarios")
public class PropietarioFrontController {

    private final PropietarioService propietarioService;

    @Autowired
    public PropietarioFrontController(PropietarioService propietarioService) {
        this.propietarioService = propietarioService;
    }

    @GetMapping("/")
    public String listarPropietarios(Model model) throws Exception {
        List<PropietarioResponseDto> list = propietarioService.getPropietarios();
        model.addAttribute("titulo", "Listado de propietarios");

        model.addAttribute("propietarios", list);
        return "/views/propietarios/listar";
    }
}
