package com.certant.vtv.controller.FrontEnd;

import com.certant.vtv.dto.InspectorRequestDto;
import com.certant.vtv.dto.InspectorResponseDto;
import com.certant.vtv.entity.Inspector;
import com.certant.vtv.service.InspectorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
@RequestMapping("/views/inspector")
@Slf4j
public class InspectorFrontController {

    private final InspectorService inspectorService;

    @Autowired
    public InspectorFrontController(InspectorService inspectorService) {
        this.inspectorService = inspectorService;
    }

    @GetMapping("/")
    public String listarInspectores(Model model) throws Exception {
        List<InspectorResponseDto> list = inspectorService.getInspectores();
        model.addAttribute("titulo", "Listado de inspectores");

        model.addAttribute("inspectores", list);
        return "/views/inspector/listar";
    }

    @GetMapping("/nuevo")
    public String nuevaInspector(){
        return "/views/inspector/frmCrear";
    }

    Integer id = 0;

    @GetMapping("/editar/{id}")
    public String editarInspector(@PathVariable("id") Integer idInspector, Model model){
        Inspector inspector = inspectorService.getInspectorById(idInspector);

        model.addAttribute("titulo", "Editar inspector");
        model.addAttribute("inspector", inspector);
        id = idInspector;
        return "/views/inspector/frmEditar";
    }

    @PostMapping("/edicion")
    public String editar(@ModelAttribute("inspector") InspectorRequestDto dto, RedirectAttributes attributes){
        if(inspectorService.existsLegajo(id, dto.getLegajo())){
            attributes.addFlashAttribute("error" , "El legajo le pertenece a otro inspector");
            return "redirect:/views/inspector/";
        }

        inspectorService.editInspector(id, dto);
        attributes.addFlashAttribute("info" , "Inspector editado con éxito");
        return "redirect:/views/inspector/";
    }

    @PostMapping("/guardar")
    public String guardar(@ModelAttribute("inspector") InspectorRequestDto dto,
                          RedirectAttributes attributes){
        if(inspectorService.existsLegajo(dto.getLegajo())){
            attributes.addFlashAttribute("error" , "No se registró al inspector - Legajo existente");
            return "redirect:/views/inspector/";
        }

        inspectorService.addInspector(dto);
        attributes.addFlashAttribute("success" , "Inspector registrado con éxito");
        return "redirect:/views/inspector/";
    }

    @GetMapping("/inactivar/{id}")
    public String ianctivarInspector(@PathVariable("id") Integer idInspector,
                                     RedirectAttributes attributes){
        inspectorService.deleteInspector(idInspector);
        attributes.addFlashAttribute("warning" , "Inspector inactivado");
        return "redirect:/views/inspector/";
    }
}
