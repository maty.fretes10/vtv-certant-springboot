package com.certant.vtv.controller.FrontEnd;

import com.certant.vtv.dto.*;
import com.certant.vtv.entity.Inspeccion;
import com.certant.vtv.entity.Marca;
import com.certant.vtv.entity.Version;
import com.certant.vtv.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@Controller
@RequestMapping("/views/inspeccion")
public class InspecccionFrontController {

    private final InspeccionesService inspeccionesService;
    private final MarcaService marcaService;
    private final ModeloService modeloService;
    private final InspectorService inspectorService;
    private final VersionService versionService;

    @Autowired
    public InspecccionFrontController(InspeccionesService inspeccionesService,
                                      MarcaService marcaService,
                                      ModeloService modeloService,
                                      InspectorService inspectorService,
                                      VersionService versionService) {
        this.inspeccionesService = inspeccionesService;
        this.marcaService = marcaService;
        this.modeloService = modeloService;
        this.inspectorService = inspectorService;
        this.versionService = versionService;
    }

    @GetMapping("/")
    public String listarInspecciones(Model model, @RequestParam(required = false) String palabra,
                                     RedirectAttributes attributes) {
        List<InspeccionResponseDto> list = inspeccionesService.getInspeccionesByPropietario(palabra);

        if(palabra != null){
            if(!palabra.isEmpty() && list.isEmpty()){
                attributes.addFlashAttribute("warning" , "Sin resultados");
                return "redirect:/views/inspeccion/";
            }
        }

        model.addAttribute("titulo", "Listado de inspecciones");
        model.addAttribute("inspecciones", list);
        model.addAttribute("palabra", palabra);
        return "/views/inspeccion/listar";
    }

    int idMarca = 0;
    int idVersion = 1;

    @GetMapping("/nuevo")
    public String nuevaInspeccion(Model model) throws Exception {
        InspeccionRequestDto inspeccion = new InspeccionRequestDto();
        List<ModeloResponseDto> modelos = modeloService.getModeloByIdMarca(idMarca);
        List<InspectorResponseDto> inspectores = inspectorService.getInspectores();
        Version version = versionService.getVersionById(idVersion);

        model.addAttribute("titulo", "Nueva inspección");
        model.addAttribute("inspeccion", inspeccion);
        model.addAttribute("inspectores", inspectores);
        model.addAttribute("modelos", modelos);
        model.addAttribute("version", version);
        return "/views/inspeccion/frmCrear";
    }

    @PostMapping("/guardar")
    public String guardar(@Valid @ModelAttribute(value="inspeccion") InspeccionRequestDto dto,
                          BindingResult result, Model model, RedirectAttributes attributes) throws Exception {
        Version version = versionService.getVersionById(dto.getVersionId());
        Marca marca = version.getModelo().getMarca();
        List<ModeloResponseDto> modelos = modeloService.getModeloByIdMarca(marca.getId());
        List<VersionResponseDto> versiones = versionService.getVersionesByIdModelo(version.getModelo().getId());
        List<InspectorResponseDto> inspectores = inspectorService.getInspectores();

        if(result.hasErrors()){
            model.addAttribute("titulo", "Nueva inspección");
            model.addAttribute("inspeccion", dto);
            model.addAttribute("inspectores", inspectores);
            model.addAttribute("modelos", modelos);
            model.addAttribute("versiones", versiones);
            model.addAttribute("version", version);
            return "/views/inspeccion/frmCrear";
        }

        inspeccionesService.addInspecciones(dto);
        attributes.addFlashAttribute("success" , "Inspección registrada con éxito");
        return "redirect:/views/inspeccion/";
    }

    @ModelAttribute("marcas")
    public List<MarcaResponseDto> getMarcas() {
        return marcaService.getMarcas();
    }

    @GetMapping(value = "/buscarPorMarca")
    public @ResponseBody List<ModeloResponseDto> todosModelosPorMarca(
            @RequestParam(value = "idMarca", required = true) int idMarca) {
        return modeloService.getModeloByIdMarca(idMarca);
    }

    @GetMapping(value = "/buscarPorModelo")
    public @ResponseBody List<VersionResponseDto> todasVersionesPorModelo(
            @RequestParam(value = "idModelo", required = true) int idModelo) {
        return versionService.getVersionesByIdModelo(idModelo);
    }

    Integer id = 0;

    @GetMapping("/editar/{id}")
    public String editarInspeccion(@PathVariable("id") Integer idInspeccion, Model model) throws Exception {
        Inspeccion inspeccion = inspeccionesService.getInspeccionById(idInspeccion);
        Integer idMarcaEdicion = inspeccion.getVehiculo().getVersion().getModelo().getMarca().getId();
        List<ModeloResponseDto> modelos = modeloService.getModeloByIdMarca(idMarcaEdicion);
        Integer idModeloEdicion= inspeccion.getVehiculo().getVersion().getModelo().getId();
        List<VersionResponseDto> versiones = versionService.getVersionesByIdModelo(idModeloEdicion);
        List<InspectorResponseDto> inspectores = inspectorService.getInspectores();

        model.addAttribute("titulo", "Editar inspección");
        model.addAttribute("inspeccion", inspeccion);
        model.addAttribute("modelos", modelos);
        model.addAttribute("versiones", versiones);
        model.addAttribute("inspectores", inspectores);
        id = idInspeccion;
        return "/views/inspeccion/frmEditar";
    }

    @PostMapping("/edicion")
    public String editar(@Valid @ModelAttribute("inspeccion") InspeccionRequestDto dto,
                         BindingResult result, Model model,
                         RedirectAttributes attributes) throws Exception {
        if(result.hasErrors()){
            Inspeccion inspeccion = inspeccionesService.getInspeccionById(id);
            Integer idMarcaEdicion = inspeccion.getVehiculo().getVersion().getModelo().getMarca().getId();
            List<ModeloResponseDto> modelos = modeloService.getModeloByIdMarca(idMarcaEdicion);
            Integer idModeloEdicion= inspeccion.getVehiculo().getVersion().getModelo().getId();
            List<VersionResponseDto> versiones = versionService.getVersionesByIdModelo(idModeloEdicion);
            List<InspectorResponseDto> inspectores = inspectorService.getInspectores();

            model.addAttribute("titulo", "Editar inspección");
            model.addAttribute("inspeccion", inspeccion);
            model.addAttribute("modelos", modelos);
            model.addAttribute("versiones", versiones);
            model.addAttribute("inspectores", inspectores);
            attributes.addFlashAttribute("error" , "No se realizaron los cambios por: " + result.getFieldError().getDefaultMessage());
            attributes.addFlashAttribute("info" , "Se reestablecieron los valores");

            return "redirect:/views/inspeccion/editar/" + id;
        }

        inspeccionesService.editInspeccion(id, dto);
        attributes.addFlashAttribute("info" , "Inspección editada con éxito");
        return "redirect:/views/inspeccion/";
    }

    @GetMapping("/inactivar/{id}")
    public String inactivarInspeccion(@PathVariable("id") Integer idInspeccion,
                                      RedirectAttributes attributes){
        inspeccionesService.inactivarInspeccion(idInspeccion);
        attributes.addFlashAttribute("warning" , "Inspección inactivada");
        return "redirect:/views/inspeccion/";
    }

}
