package com.certant.vtv.controller;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.certant.vtv.dto.PersonaRequestDto;
import com.certant.vtv.dto.PersonaResponseDto;
import com.certant.vtv.service.PersonaService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1")

public class PersonaController {
	private final PersonaService personaService;
	
	@Autowired
	public PersonaController(PersonaService personaService) {
		this.personaService = personaService;
	}
	
	@GetMapping("/personas")
	public List<PersonaResponseDto> getPersonas() throws Exception{
		return personaService.getPersonas();
	}
	
	@PostMapping("/persona")
	@ResponseStatus(HttpStatus.CREATED)
	public void addPersona(@Valid @RequestBody PersonaRequestDto dto) {
		personaService.addPersona(dto);
	}
}
