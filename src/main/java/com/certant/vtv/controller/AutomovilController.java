package com.certant.vtv.controller;

import com.certant.vtv.dto.AutomovilRequestDto;
import com.certant.vtv.dto.AutomovilResponseDto;
import com.certant.vtv.service.AutomovilService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1")

public class AutomovilController {
	private final AutomovilService automovilService;

	@Autowired
	public AutomovilController(AutomovilService automovilService) {
		this.automovilService = automovilService;
	}

	@PostMapping("/automovil")
	@ResponseStatus(HttpStatus.CREATED)
	public void addAutomovil(@Valid @RequestBody AutomovilRequestDto dto) {
		automovilService.addAutomovil(dto);
	}

	@GetMapping("/automoviles")
	public List<AutomovilResponseDto> getAutomoviles(){
		return automovilService.getAutomoviles();
	}

	@PostMapping("/automovil/edit")
	public void editAutomovil(@RequestParam Integer id, @Valid @RequestBody AutomovilRequestDto requestDto){
		automovilService.editAutomovil(id, requestDto);
	}

	@DeleteMapping("/automovil/inactivar")
	public void inactivarAutomovil(@RequestParam Integer id){
		automovilService.deleteAutomovil(id);
	}
}
