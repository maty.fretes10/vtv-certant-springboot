package com.certant.vtv.controller;

import com.certant.vtv.dto.VehiculoResponseDto;
import com.certant.vtv.service.VehiculoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1")

public class VehiculoController {
	private final VehiculoService vehiculoService;

	@Autowired
	public VehiculoController(VehiculoService vehiculoService) {
		this.vehiculoService = vehiculoService;
	}

	@GetMapping("/vehiculos")
	public List<VehiculoResponseDto> getVehiculosByEstado(@RequestParam(required = false)String estado ){
		return vehiculoService.getVehiculosByEstado(estado);
	}
}
