package com.certant.vtv.controller;

import com.certant.vtv.dto.VersionRequestDto;
import com.certant.vtv.dto.VersionResponseDto;
import com.certant.vtv.service.VersionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1")

public class VersionController {
	private final VersionService versionService;

	@Autowired
	public VersionController(VersionService versionService) {
		this.versionService = versionService;
	}

	@PostMapping("/version")
	@ResponseStatus(HttpStatus.CREATED)
	public void addVersion(@Valid @RequestBody VersionRequestDto dto) {
		versionService.addVersion(dto);
	}

	@GetMapping("/versiones")
	public List<VersionResponseDto> getVersiones(@RequestParam Integer id){
		return versionService.getVersionesByIdModelo(id);
	}

	@PostMapping("/version/edit")
	public void editVersion(@RequestParam Integer id, @Valid @RequestBody VersionRequestDto dto){
		versionService.editVersion(id, dto);
	}
}
