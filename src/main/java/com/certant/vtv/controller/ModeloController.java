package com.certant.vtv.controller;

import com.certant.vtv.dto.*;
import com.certant.vtv.service.ModeloService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1")

public class ModeloController {
	private final ModeloService modeloService;

	@Autowired
	public ModeloController(ModeloService modeloService) {
		this.modeloService = modeloService;
	}

	@PostMapping("/modelo")
	@ResponseStatus(HttpStatus.CREATED)
	public void addModelo(@Valid @RequestBody ModeloRequestDto dto) {
		modeloService.addModelo(dto);
	}

	@GetMapping("/modelos")
	public List<ModeloResponseDto> getModelos(@RequestParam Integer id){
		return modeloService.getModeloByIdMarca(id);
	}

	@PostMapping("/modelo/edit")
	public void editModelo(@RequestParam Integer id, @Valid @RequestBody ModeloRequestDto dto){
		modeloService.editModelo(id, dto);
	}
}
