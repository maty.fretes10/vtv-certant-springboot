package com.certant.vtv.controller;

import com.certant.vtv.dto.MarcaRequestDto;
import com.certant.vtv.dto.MarcaResponseDto;
import com.certant.vtv.service.MarcaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1")

public class MarcaController {
	private final MarcaService marcaService;

	@Autowired
	public MarcaController(MarcaService marcaService) {
		this.marcaService = marcaService;
	}

	@PostMapping("/marca")
	@ResponseStatus(HttpStatus.CREATED)
	public void addMarca(@Valid @RequestBody MarcaRequestDto dto) {
		marcaService.addMarca(dto);
	}
	
	@GetMapping("/marcas")
	public List<MarcaResponseDto> getMarcas(){
		return marcaService.getMarcas();
	}

	@PostMapping("/marca/edit")
	public void editMarca(@RequestParam Integer id, @Valid @RequestBody MarcaRequestDto dto){
		marcaService.editMarca(id, dto);
	}
}
