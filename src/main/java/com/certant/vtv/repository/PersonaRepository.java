package com.certant.vtv.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.certant.vtv.entity.Persona;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonaRepository extends  JpaRepository<Persona, Integer>{

}
