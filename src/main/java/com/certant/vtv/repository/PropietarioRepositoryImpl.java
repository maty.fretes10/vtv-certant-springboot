package com.certant.vtv.repository;

import com.certant.vtv.entity.Propietario;
import org.springframework.stereotype.Repository;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class PropietarioRepositoryImpl {
    @PersistenceContext
    EntityManager entityManager;

    public List<Propietario> findPropietariosByFilter(String nombre, String apellido) {

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Propietario> cq = criteriaBuilder.createQuery(Propietario.class);

        Root<Propietario> prop = cq.from(Propietario.class);
        List<Predicate> predicates = new ArrayList<>();

        if (nombre != null && !nombre.isEmpty()) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.lower(prop.get("nombre")), "%" + nombre.toLowerCase() + "%"));
        }
        if (apellido != null && !apellido.isEmpty()) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.lower(prop.get("apellido")), "%" + apellido.toLowerCase() + "%"));
        }

        cq.where(predicates.toArray(new Predicate[0]));

        TypedQuery<Propietario> query = entityManager.createQuery(cq);
        return query.getResultList();
    }
}
