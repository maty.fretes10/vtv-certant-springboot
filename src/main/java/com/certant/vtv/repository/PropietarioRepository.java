package com.certant.vtv.repository;

import com.certant.vtv.entity.Propietario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PropietarioRepository extends  JpaRepository<Propietario, Integer>{
    @Query("SELECT p FROM Propietario p WHERE p.dni=:dni")
    Optional<Propietario> findByDNI(String dni);

    @Query("SELECT p FROM Propietario p WHERE p.nombre LIKE %?1% OR p.apellido LIKE  %?1% OR p.dni LIKE  %?1%")
    List<Propietario> findAllByPalabraClave(String palabra);
}
