package com.certant.vtv.repository;

import com.certant.vtv.entity.Vehiculo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface VehiculoRepository extends JpaRepository<Vehiculo, Integer> {
    @Query("SELECT v FROM Vehiculo v WHERE v.estado=:estado")
    List<Vehiculo> findByEstado(String estado);

    @Query("SELECT v FROM Vehiculo v WHERE v.persona.id=:idPersona")
    List<Vehiculo> findByPropietario(Integer idPersona);

}
