package com.certant.vtv.repository;

import com.certant.vtv.entity.Automovil;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface AutomovilRepository extends JpaRepository<Automovil, Integer> {
}
