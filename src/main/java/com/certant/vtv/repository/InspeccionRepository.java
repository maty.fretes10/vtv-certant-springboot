package com.certant.vtv.repository;

import com.certant.vtv.entity.Inspeccion;
import com.certant.vtv.entity.Modelo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InspeccionRepository extends  JpaRepository<Inspeccion, Integer>{
}
