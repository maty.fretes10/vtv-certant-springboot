package com.certant.vtv.repository;

import com.certant.vtv.entity.Version;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VersionRepository extends JpaRepository<Version, Integer> {
    @Query("SELECT v FROM Version v WHERE v.modelo.id=:modeloId")
    List<Version> findByModeloId(Integer modeloId);
}
