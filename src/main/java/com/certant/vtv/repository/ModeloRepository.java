package com.certant.vtv.repository;

import com.certant.vtv.entity.Modelo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ModeloRepository extends JpaRepository<Modelo, Integer> {
    @Query("SELECT m FROM Modelo m WHERE m.marca.id=:marcaId")
    List<Modelo> findByMarcaId(Integer marcaId);
}
