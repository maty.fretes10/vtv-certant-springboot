package com.certant.vtv.service;

import com.certant.vtv.dto.InspectorRequestDto;
import com.certant.vtv.dto.InspectorResponseDto;
import com.certant.vtv.entity.Inspector;

import java.util.List;

public interface InspectorService {

	void addInspector(InspectorRequestDto inspectorRequestDto);
	
	List<InspectorResponseDto> getInspectores() throws Exception;

	void deleteInspector(Integer id);

	void editInspector(Integer id, InspectorRequestDto inspectorRequestDto);

	Inspector getInspectorById(Integer id);

	InspectorResponseDto getInspectorByIdResponse(Integer id);

	boolean existsLegajo(String legajo);
	boolean existsLegajo(Integer id, String legajo);
}
