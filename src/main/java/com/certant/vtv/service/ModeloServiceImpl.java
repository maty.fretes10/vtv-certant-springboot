package com.certant.vtv.service;

import com.certant.vtv.dto.ModeloRequestDto;
import com.certant.vtv.dto.ModeloResponseDto;
import com.certant.vtv.entity.Marca;
import com.certant.vtv.entity.Modelo;
import com.certant.vtv.repository.ModeloRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ModeloServiceImpl implements ModeloService{


    private final ModeloRepository modeloRepository;
    private final ModelMapper modelMapper;
    private final MarcaService marcaService;

    @Autowired
    public ModeloServiceImpl(ModeloRepository modeloRepository,
							 ModelMapper modelMapper,
							 MarcaService marcaService) {
        this.modeloRepository = modeloRepository;
        this.modelMapper = modelMapper;
        this.marcaService = marcaService;
    }


	@Override
	public void addModelo(ModeloRequestDto modeloRequestDto) {
		Marca marca = marcaService.getMarcaById(modeloRequestDto.getMarcaId());

		Modelo entity = new Modelo();
		entity.setDescripcion(modeloRequestDto.getDescripcion());
		entity.setMarca(marca);

		modeloRepository.save(entity);
	}

	@Override
	public List<ModeloResponseDto> getModeloByIdMarca(Integer marcaID) {
		List<Modelo> allModelosPorMarca = modeloRepository.findByMarcaId(marcaID);

		return allModelosPorMarca.stream()
				.parallel()
				.map(modelo -> modelMapper.map(modelo, ModeloResponseDto.class))
				.collect(Collectors.toList());
	}

	@Override
	public void deleteModelo(Integer id) {

	}

	@Override
	public void editModelo(Integer id, ModeloRequestDto modeloRequestDto) {
		Modelo modelo = getModeloById(id);
		modelo.setDescripcion(modeloRequestDto.getDescripcion());
		modelo.setMarca(marcaService.getMarcaById(modeloRequestDto.getMarcaId()));

		modeloRepository.save(modelo);
	}

	@Override
	public Modelo getModeloById(Integer id) {
		Optional<Modelo> optional = modeloRepository.findById(id);

		if(!optional.isPresent()){
			throw  new RuntimeException("El modelo no esta presente");
		}

		return optional.get();
	}
}
