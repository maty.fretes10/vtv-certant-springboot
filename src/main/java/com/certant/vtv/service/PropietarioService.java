package com.certant.vtv.service;

import com.certant.vtv.dto.PropietarioRequestDto;
import com.certant.vtv.dto.PropietarioResponseDto;
import com.certant.vtv.entity.Propietario;

import java.util.List;

public interface PropietarioService {

	Propietario addPropietario(PropietarioRequestDto propietarioRequestDto);
	
	List<PropietarioResponseDto> getPropietarios() throws Exception;

	void inactivarPropietario(Integer id);

	Propietario editPropietario(Integer id, PropietarioRequestDto propietarioRequestDto);

	Propietario getPropietarioById(Integer id);

	Propietario decidirAgregarOEditar(PropietarioRequestDto propietarioRequestDto);

	Integer getIdbyDni(String dni);

	void activarPropietario(Integer id);
}
