package com.certant.vtv.service;

import com.certant.vtv.dto.MarcaRequestDto;
import com.certant.vtv.dto.MarcaResponseDto;
import com.certant.vtv.entity.Marca;
import com.certant.vtv.repository.MarcaRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class MarcaServiceImpl implements MarcaService{


    private final MarcaRepository marcaRepository;
    private final ModelMapper modelMapper;

    @Autowired
    public MarcaServiceImpl(MarcaRepository marcaRepository, ModelMapper modelMapper) {
        this.marcaRepository = marcaRepository;
        this.modelMapper = modelMapper;
    }

	@Override
	public void addMarca(MarcaRequestDto marcaRequestDto) {
		Marca entity = new Marca();
		entity.setDescripcion(marcaRequestDto.getDescripcion());

		marcaRepository.save(entity);
	}

	@Override
	public List<MarcaResponseDto> getMarcas() {
		List<Marca> allMarcas = marcaRepository.findAll();

		return allMarcas.stream()
				.parallel()
				.map(marca -> modelMapper.map(marca, MarcaResponseDto.class))
				.collect(Collectors.toList());
	}

	@Override
	public void deleteMarca(Integer id) {

	}

	@Override
	public void editMarca(Integer id, MarcaRequestDto marcaRequestDto) {
		Marca marca = getMarcaById(id);
		marca.setDescripcion(marcaRequestDto.getDescripcion());

		marcaRepository.save(marca);
	}

	@Override
	public Marca getMarcaById(Integer id) {
		Optional<Marca> optional = marcaRepository.findById(id);

		if(!optional.isPresent()){
			throw  new RuntimeException("La marca no esta presente");
		}

		return optional.get();
	}


}
