package com.certant.vtv.service;

import com.certant.vtv.constants.Estado;
import com.certant.vtv.dto.InspeccionRequestDto;
import com.certant.vtv.dto.InspeccionResponseDto;
import com.certant.vtv.entity.Inspeccion;

import java.util.List;

public interface InspeccionesService {
	void addInspecciones(InspeccionRequestDto inspeccionRequestDto);

	List<InspeccionResponseDto> getInspecciones() ;

	List<InspeccionResponseDto> getInspeccionesByPropietario(String palabra);

	void editInspeccion(Integer id, InspeccionRequestDto inspeccionRequestDto);

	Inspeccion getInspeccionById(Integer id);

	void inactivarInspeccion(Integer id);

	String decidirVencimiento(Estado estadoFinal);

	Estado decidirEstadoFinal(Estado resultadoMediciones, Estado resultadoObservaciones);
}
