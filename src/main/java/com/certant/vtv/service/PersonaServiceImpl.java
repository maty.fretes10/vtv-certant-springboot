package com.certant.vtv.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.certant.vtv.dto.PersonaRequestDto;
import com.certant.vtv.dto.PersonaResponseDto;
import com.certant.vtv.entity.Persona;
import com.certant.vtv.repository.PersonaRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PersonaServiceImpl implements PersonaService{

	
    private final PersonaRepository personaRepository;
    private final ModelMapper modelMapper;

    @Autowired
    public PersonaServiceImpl(PersonaRepository personaRepository, ModelMapper modelMapper) {
        this.personaRepository = personaRepository;
        this.modelMapper = modelMapper;
    }
    
    
	@Override
	public void addPersona(PersonaRequestDto personaRequestDto) {
		Persona entity = modelMapper.map(personaRequestDto, Persona.class);
		personaRepository.save(entity);
	}

	@Override
	public List<PersonaResponseDto> getPersonas() throws Exception {
		List<Persona> allPersonas = personaRepository.findAll();

		try {
			return allPersonas.stream()
					.parallel()
					.map(persona -> modelMapper.map(persona, PersonaResponseDto.class))
					.collect(Collectors.toList());
		} catch (Exception e) {
			throw new Exception();
		}
		
	}

	@Override
	public Persona getPersonaById(Integer id) {
		Optional<Persona> optional = personaRepository.findById(id);

		if(!optional.isPresent()){
			throw  new RuntimeException("La persona no esta presente");
		}

		return optional.get();
	}

	@Override
	public void inactivarPersona(Integer id){
    	Persona persona = getPersonaById(id);
    	persona.setActivo(false);
    	personaRepository.save(persona);
	}

}
