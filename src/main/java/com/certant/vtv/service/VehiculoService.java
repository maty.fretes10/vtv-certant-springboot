package com.certant.vtv.service;

import com.certant.vtv.dto.VehiculoResponseDto;

import java.util.List;

public interface VehiculoService {

	List<VehiculoResponseDto> getVehiculosByEstado(String estado) ;

	boolean vehiculosActivosPorPropietario(Integer idPropietario);

}
