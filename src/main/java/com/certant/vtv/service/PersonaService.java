package com.certant.vtv.service;

import java.util.List;
import com.certant.vtv.dto.PersonaRequestDto;
import com.certant.vtv.dto.PersonaResponseDto;
import com.certant.vtv.entity.Persona;

public interface PersonaService {

	void addPersona(PersonaRequestDto personaRequestDto);
	
	List<PersonaResponseDto> getPersonas() throws Exception;

	Persona getPersonaById(Integer id);

	void inactivarPersona(Integer id);
}
