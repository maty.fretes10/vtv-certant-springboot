package com.certant.vtv.service;

import com.certant.vtv.constants.Estado;
import com.certant.vtv.dto.AutomovilRequestDto;
import com.certant.vtv.dto.InspeccionRequestDto;
import com.certant.vtv.dto.InspeccionResponseDto;
import com.certant.vtv.entity.*;
import com.certant.vtv.exception.ResourceNotFoundException;
import com.certant.vtv.repository.InspeccionRepository;
import com.certant.vtv.repository.PropietarioRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class InspeccionesServiceImpl implements InspeccionesService{

    private final InspeccionRepository inspeccionRepository;
    private final AutomovilService automovilService;
    private final InspectorService inspectorService;
    private final ModelMapper modelMapper;
    private final PropietarioRepository propietarioRepository;

    @Autowired
    public InspeccionesServiceImpl (InspeccionRepository inspeccionRepository,
                                    AutomovilService automovilService,
                                    InspectorService inspectorService,
                                    ModelMapper modelMapper,
                                    PropietarioRepository propietarioRepository){
        this.inspeccionRepository = inspeccionRepository;
        this.automovilService = automovilService;
        this.inspectorService = inspectorService;
        this.modelMapper = modelMapper;
        this.propietarioRepository = propietarioRepository;
    }

    @Override
    public void addInspecciones(InspeccionRequestDto inspeccionRequestDto) {

        Inspector inspector = inspectorService.getInspectorById(inspeccionRequestDto.getPersonaId());

        Inspeccion inspeccion = new Inspeccion();
        inspeccion.setFecha(LocalDate.now());
        inspeccion.setEstadoObservaciones(Estado.valueOf(inspeccionRequestDto.getEstadoObservaciones()));
        inspeccion.setEstadoMediciones(Estado.valueOf(inspeccionRequestDto.getEstadoMediciones()));
        inspeccion.setEstadoFinal(decidirEstadoFinal(inspeccion.getEstadoMediciones(), inspeccion.getEstadoObservaciones()));
        inspeccion.setExento(inspeccionRequestDto.isExento());
        inspeccion.setActivo(true);

        AutomovilRequestDto automovilRequestDto = new AutomovilRequestDto();
        automovilRequestDto.setPatente(inspeccionRequestDto.getPatente());
        automovilRequestDto.setVto(decidirVencimiento(inspeccion.getEstadoFinal()));
        automovilRequestDto.setEstado(inspeccion.getEstadoFinal().toString());
        automovilRequestDto.setVersionId(inspeccionRequestDto.getVersionId());
        automovilRequestDto.setNombre(inspeccionRequestDto.getNombre());
        automovilRequestDto.setApellido(inspeccionRequestDto.getApellido());
        automovilRequestDto.setDni(inspeccionRequestDto.getDni());
        automovilRequestDto.setTelefono(inspeccionRequestDto.getTelefono());
        automovilRequestDto.setCorreo(inspeccionRequestDto.getCorreo());
        Automovil automovil = automovilService.decidirSiSeAgrega(automovilRequestDto);

        inspeccion.setPersona(inspector);
        inspeccion.setVehiculo(automovil);

        inspeccionRepository.save(inspeccion);
    }

    @Override
    public String decidirVencimiento(Estado estadoFinal) {
        if(estadoFinal.equals(Estado.APTO)){
            return LocalDate.now().plusYears(1).toString();
        }
        return LocalDate.now().toString();
    }

    @Override
    public Estado decidirEstadoFinal(Estado resultadoMediciones, Estado resultadoObservaciones) {
        if (resultadoObservaciones != null && resultadoMediciones != null) {
            if (resultadoObservaciones.equals(Estado.RECHAZADO) || resultadoMediciones.equals(Estado.RECHAZADO)) {
                return Estado.RECHAZADO;
            } else if (resultadoObservaciones.equals(Estado.CONDICIONAL)
                    || resultadoMediciones.equals(Estado.CONDICIONAL)) {
                return Estado.CONDICIONAL;
            } else if (resultadoObservaciones.equals(Estado.APTO) && resultadoMediciones.equals(Estado.APTO)) {
                return Estado.APTO;
            }
        }
        return null;
    }

    @Override
    public List<InspeccionResponseDto> getInspecciones() {
        List<Inspeccion> allInspecciones = inspeccionRepository.findAll();
        List<Inspeccion> inspeccionesActivas = new ArrayList<>();

        for (Inspeccion inspeccion : allInspecciones) {
            if(inspeccion.isActivo())
                inspeccionesActivas.add(inspeccion);
        }

        return inspeccionesActivas.stream()
                .parallel()
                .map(insp -> modelMapper.map(insp, InspeccionResponseDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public List<InspeccionResponseDto> getInspeccionesByPropietario(String palabra) {
        if(palabra == null || palabra.isEmpty()){
            return getInspecciones();
        }

        List<Propietario> propietariosByFiltros = propietarioRepository.findAllByPalabraClave(palabra);
        List<Inspeccion> allInspecciones = inspeccionRepository.findAll();
        List<Inspeccion> inspeccionesByPropietario = new ArrayList<>();
        List<Inspeccion> inspeccionesActivas = new ArrayList<>();

        for (Inspeccion inspeccion: allInspecciones) {
            Persona persona = inspeccion.getVehiculo().getPersona();
            for (Propietario propietario : propietariosByFiltros) {
                if (persona.equals(propietario)) {
                    inspeccionesByPropietario.add(inspeccion);
                }
            }
        }

        for (Inspeccion inspeccion : inspeccionesByPropietario) {
            if(inspeccion.isActivo())
                inspeccionesActivas.add(inspeccion);
        }

        return inspeccionesActivas.stream()
            .parallel()
            .map(inspeccion -> modelMapper.map(inspeccion, InspeccionResponseDto.class))
            .collect(Collectors.toList());
    }



    @Override
    public void editInspeccion(Integer id, InspeccionRequestDto inspeccionRequestDto) {
        Inspector inspector = inspectorService.getInspectorById(inspeccionRequestDto.getPersonaId());
        Inspeccion inspeccion = getInspeccionById(id);
        inspeccion.setFecha(LocalDate.now());
        inspeccion.setEstadoObservaciones(Estado.valueOf(inspeccionRequestDto.getEstadoObservaciones()));
        inspeccion.setEstadoMediciones(Estado.valueOf(inspeccionRequestDto.getEstadoMediciones()));
        inspeccion.setEstadoFinal(decidirEstadoFinal(inspeccion.getEstadoMediciones(), inspeccion.getEstadoObservaciones()));
        inspeccion.setExento(inspeccionRequestDto.isExento());
        inspeccion.setPersona(inspector);

        Automovil vehiculo = (Automovil) inspeccion.getVehiculo();

        AutomovilRequestDto automovilRequestDto = new AutomovilRequestDto();
        automovilRequestDto.setPatente(inspeccionRequestDto.getPatente());
        automovilRequestDto.setVto(decidirVencimiento(inspeccion.getEstadoFinal()));
        automovilRequestDto.setEstado(inspeccion.getEstadoFinal().toString());
        automovilRequestDto.setVersionId(inspeccionRequestDto.getVersionId());
        automovilRequestDto.setNombre(inspeccionRequestDto.getNombre());
        automovilRequestDto.setApellido(inspeccionRequestDto.getApellido());
        automovilRequestDto.setDni(inspeccionRequestDto.getDni());
        automovilRequestDto.setTelefono(inspeccionRequestDto.getTelefono());
        automovilRequestDto.setCorreo(inspeccionRequestDto.getCorreo());

        automovilService.editAutomovil(vehiculo.getId(), automovilRequestDto);

        inspeccionRepository.save(inspeccion);
    }

    @Override
    public Inspeccion getInspeccionById(Integer id) {
        Optional<Integer> optionalId = Optional.ofNullable(id);
        if (!optionalId.isPresent()) {
            throw new IllegalStateException("Id de la inspeccion es null");
        }

        Optional<Inspeccion> optional = inspeccionRepository.findById(id);
        if(!optional.isPresent()){
            throw  new ResourceNotFoundException("La inspeccion no esta presente");
        }
        return optional.get();
    }

    @Override
    public void inactivarInspeccion(Integer id) {
        Inspeccion inspeccion = getInspeccionById(id);
        if(!inspeccion.isActivo()){
            throw new IllegalArgumentException("La inspeccion ya se encuentra inactiva");
        }
        inspeccion.setActivo(false);
        inspeccionRepository.save(inspeccion);
    }
}
