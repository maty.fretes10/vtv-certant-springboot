package com.certant.vtv.service;

import com.certant.vtv.constants.TipoVehiculos;
import com.certant.vtv.dto.AutomovilRequestDto;
import com.certant.vtv.dto.AutomovilResponseDto;
import com.certant.vtv.dto.PropietarioRequestDto;
import com.certant.vtv.entity.*;
import com.certant.vtv.exception.ResourceNotFoundException;
import com.certant.vtv.repository.AutomovilRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class AutomovilesServiceImpl implements AutomovilService {

    private final AutomovilRepository automovilRepository;
    private final ModelMapper modelMapper;
    private final VersionService versionService;
    private final PropietarioService propietarioService;
    private final VehiculoService vehiculoService;

    @Autowired
    public AutomovilesServiceImpl(AutomovilRepository automovilRepository,
								  ModelMapper modelMapper,
								  VersionService versionService,
								  PropietarioService propietarioService,
								  VehiculoService vehiculoService) {
        this.automovilRepository = automovilRepository;
        this.modelMapper = modelMapper;
        this.versionService = versionService;
        this.propietarioService = propietarioService;
        this.vehiculoService = vehiculoService;
    }

	@Override
	public Automovil addAutomovil(AutomovilRequestDto automovilRequestDto) {
		Vehiculo vehiculo = new Vehiculo();
		vehiculo.setPatente(automovilRequestDto.getPatente());
		vehiculo.setVto(LocalDate.parse(automovilRequestDto.getVto()));
		vehiculo.setEstado(automovilRequestDto.getEstado());
		vehiculo.setTipo(TipoVehiculos.AUTOMOVIL);
		vehiculo.setVersion(versionService.getVersionById(automovilRequestDto.getVersionId()));
		vehiculo.setActivo(true);

		PropietarioRequestDto nuevoPropietario = new PropietarioRequestDto();
		nuevoPropietario.setNombre(automovilRequestDto.getNombre());
		nuevoPropietario.setApellido(automovilRequestDto.getApellido());
		nuevoPropietario.setCorreo(automovilRequestDto.getCorreo());
		nuevoPropietario.setTelefono(automovilRequestDto.getTelefono());
		nuevoPropietario.setDni(automovilRequestDto.getDni());

		Persona persona = propietarioService.decidirAgregarOEditar(nuevoPropietario);

		Automovil entity = new Automovil(
				vehiculo.getId(),
				vehiculo.getPatente(),
				vehiculo.getVto(),
				vehiculo.getEstado(),
				vehiculo.getVersion(),
				persona,
				vehiculo.isActivo()
		);

		return automovilRepository.save(entity);
	}

	@Override
	public Automovil decidirSiSeAgrega(AutomovilRequestDto automovilRequestDto) {
		Automovil automovil = automovilPorPatente(automovilRequestDto.getPatente());
		if(automovil == null){
			log.info("Se crea nuevo vehiculo");
			return addAutomovil(automovilRequestDto);
		}
		if(!automovil.isActivo()){
			automovil.setActivo(true);
		}

		propietarioService.activarPropietario(automovil.getPersona().getId());
		log.info ("vehiculo ya existente");
		return automovil;
	}

	@Override
	public List<AutomovilResponseDto> getAutomoviles(){
		List<Automovil> allautomoviles = automovilRepository.findAll();
		List<Automovil> allActivos = new ArrayList<>();

		for (Automovil automovil: allautomoviles) {
			if(automovil.isActivo()){
				allActivos.add(automovil);
			}
		}

		return allActivos.stream()
				.parallel()
				.map(auto -> modelMapper.map(auto, AutomovilResponseDto.class))
				.collect(Collectors.toList());
	}

	public Automovil getAutomovilId(Integer id){
		Optional<Integer> optionalId = Optional.ofNullable(id);
		if (!optionalId.isPresent()) {
			throw new IllegalStateException("Id del vehiculo es null");
		}
		Optional<Automovil> optional = automovilRepository.findById(id);
		if(!optional.isPresent()){
			throw  new ResourceNotFoundException("El automovil no esta presente");
		}
		return optional.get();
	}

	@Override
	public Automovil editAutomovil(Integer id, AutomovilRequestDto automovilRequestDto) {
		Automovil automovil = getAutomovilId(id);
		automovil.setPatente(automovilRequestDto.getPatente());
		automovil.setVto(LocalDate.parse(automovilRequestDto.getVto()));
		automovil.setEstado(automovilRequestDto.getEstado());
		automovil.setVersion(versionService.getVersionById(automovilRequestDto.getVersionId()));

		Propietario persona = getPropietarioVehiculo(automovil.getId());
		PropietarioRequestDto propietarioRequestDto = new PropietarioRequestDto();
		propietarioRequestDto.setNombre(automovilRequestDto.getNombre());
		propietarioRequestDto.setApellido(automovilRequestDto.getApellido());
		propietarioRequestDto.setCorreo(automovilRequestDto.getCorreo());
		propietarioRequestDto.setTelefono(automovilRequestDto.getTelefono());
		propietarioRequestDto.setDni(automovilRequestDto.getDni());
		propietarioService.decidirAgregarOEditar(propietarioRequestDto);

		return automovilRepository.save(automovil);

	}

	@Override
	public void deleteAutomovil(Integer id) {
		Automovil automovil = getAutomovilId(id);
		if(!automovil.isActivo()){
			throw new IllegalArgumentException("El vehiculo ya se encuentra inactivo");
		}

		automovil.setActivo(false);
		automovilRepository.save(automovil);

		if(!vehiculoService.vehiculosActivosPorPropietario(automovil.getPersona().getId())){
			propietarioService.inactivarPropietario(automovil.getPersona().getId());
		}
    }

	@Override
	public Automovil getAutomovilById(Integer id) {
		Optional<Automovil> optional = automovilRepository.findById(id);
		if(!optional.isPresent()){
			throw  new RuntimeException("El vehiculo no esta presente");
		}
		return optional.get();
	}

	@Override
	public Propietario getPropietarioVehiculo(Integer idVehiculo){
    	Vehiculo vehiculo = getAutomovilId(idVehiculo);
    	return (Propietario) vehiculo.getPersona();
	}

	@Override
	public Automovil automovilPorPatente(String patente){
		List<Automovil> allautomoviles = automovilRepository.findAll();
		Automovil auto = null;
		for (Automovil automovil: allautomoviles) {
			if(automovil.getPatente().equals(patente)){
				auto = automovil;
			}
		}

		return auto;
	}


}
