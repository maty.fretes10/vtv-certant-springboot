package com.certant.vtv.service;

import com.certant.vtv.dto.VersionRequestDto;
import com.certant.vtv.dto.VersionResponseDto;
import com.certant.vtv.entity.Version;

import java.util.List;

public interface VersionService {

	void addVersion(VersionRequestDto versionRequestDto);
	
	List<VersionResponseDto> getVersionesByIdModelo(Integer modelID);

	Version getVersionById(Integer id);

	void editVersion(Integer id, VersionRequestDto versionRequestDto);
}
