package com.certant.vtv.service;

import com.certant.vtv.dto.VehiculoResponseDto;
import com.certant.vtv.entity.Vehiculo;
import com.certant.vtv.repository.VehiculoRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class VehiculoServiceImpl implements VehiculoService {

    private final VehiculoRepository vehiculoRepository;
    private final ModelMapper modelMapper;

    @Autowired
    public VehiculoServiceImpl(VehiculoRepository vehiculoRepository,
                               ModelMapper modelMapper) {
        this.vehiculoRepository = vehiculoRepository;
        this.modelMapper = modelMapper;
    }


	@Override
	public List<VehiculoResponseDto> getVehiculosByEstado(String estado){
		List<Vehiculo> allVehiculosByEstado = vehiculoRepository.findByEstado(estado);
		List<Vehiculo> allActivos = new ArrayList<>();

		for(Vehiculo vehiculo : allVehiculosByEstado){
			if(vehiculo.isActivo()){
				allActivos.add(vehiculo);
			}
		}

		if(allActivos.isEmpty()){
			List<Vehiculo> allVehiculos = vehiculoRepository.findAll();
			for(Vehiculo vehiculo : allVehiculos){
				if(vehiculo.isActivo()){
					allActivos.add(vehiculo);
				}
			}

		}

		return allActivos.stream()
				.parallel()
				.map(vehiculo -> modelMapper.map(vehiculo, VehiculoResponseDto.class))
				.collect(Collectors.toList());
	}

	@Override
	public boolean vehiculosActivosPorPropietario(Integer idPropietario) {
		List<Vehiculo> vehiculosPorPropietario = vehiculoRepository.findByPropietario(idPropietario);
		boolean retorno = false;

		for (Vehiculo vehiculo : vehiculosPorPropietario) {
			if(vehiculo.isActivo()){
				retorno = true;
			}
		}

		return retorno;
	}

}
