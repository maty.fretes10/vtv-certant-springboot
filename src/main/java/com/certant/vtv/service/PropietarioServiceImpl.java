package com.certant.vtv.service;

import com.certant.vtv.constants.TipoPersonas;
import com.certant.vtv.dto.PropietarioRequestDto;
import com.certant.vtv.dto.PropietarioResponseDto;
import com.certant.vtv.entity.Persona;
import com.certant.vtv.entity.Propietario;
import com.certant.vtv.repository.PropietarioRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class PropietarioServiceImpl implements PropietarioService{


    private final PropietarioRepository propietarioRepository;
    private final ModelMapper modelMapper;

    @Autowired
    public PropietarioServiceImpl(PropietarioRepository propietarioRepository, ModelMapper modelMapper) {
        this.propietarioRepository = propietarioRepository;
        this.modelMapper = modelMapper;
    }
    
    
	@Override
	public Propietario addPropietario(PropietarioRequestDto propietarioRequestDto) {
		Persona persona = new Persona();
		persona.setNombre(propietarioRequestDto.getNombre());
		persona.setApellido(propietarioRequestDto.getApellido());
		persona.setDni(propietarioRequestDto.getDni());
		persona.setTipo(TipoPersonas.PROPIETARIO);
		persona.setActivo(true);

		Propietario entity = new Propietario(
				persona.getId(),
				persona.getNombre(),
				persona.getApellido(),
				persona.getDni(),
				propietarioRequestDto.getCorreo(),
				propietarioRequestDto.getTelefono(),
				persona.isActivo()
		);

		return propietarioRepository.save(entity);
	}

	@Override
	public List<PropietarioResponseDto> getPropietarios() throws Exception {
		List<Propietario> allPropietarios = propietarioRepository.findAll();
		List<Propietario> allPropietariosActivos = new ArrayList<>();

		for (Propietario prop: allPropietarios) {
			if(prop.isActivo()){
				allPropietariosActivos.add(prop);
			}
		}
		return allPropietariosActivos.stream()
					.parallel()
					.map(propietario -> modelMapper.map(propietario, PropietarioResponseDto.class))
					.collect(Collectors.toList());
	}

	@Override
	public void inactivarPropietario(Integer id) {
		Propietario propietario = getPropietarioById(id);
		propietario.setActivo(false);

		propietarioRepository.save(propietario);
	}

	@Override
	public Propietario editPropietario(Integer id, PropietarioRequestDto propietarioRequestDto) {
		Propietario propietario = getPropietarioById(id);
		propietario.setNombre(propietarioRequestDto.getNombre());
		propietario.setApellido(propietarioRequestDto.getApellido());
		propietario.setDni(propietarioRequestDto.getDni());
		propietario.setTelefono(propietarioRequestDto.getTelefono());
		propietario.setCorreo(propietarioRequestDto.getCorreo());

		return propietarioRepository.save(propietario);
	}

	@Override
	public Propietario getPropietarioById(Integer id) {
		Optional<Propietario> optional = propietarioRepository.findById(id);

		if(!optional.isPresent()){
			throw  new RuntimeException("Propietario no esta presente");
		}

		return optional.get();
	}

	@Override
	public Integer getIdbyDni(String dni) {
		Optional<Propietario> optional = propietarioRepository.findByDNI(dni);
		int retorno = -1;
		if(optional.isPresent()){
			retorno = optional.get().getId();
		}
		return retorno;
	}

	@Override
	public void activarPropietario(Integer id) {
		Propietario propietario = getPropietarioById(id);
		propietario.setActivo(true);
		propietarioRepository.save(propietario);
	}

	@Override
	public Propietario decidirAgregarOEditar(PropietarioRequestDto propietarioRequestDto) {
		Optional<Propietario> optional = propietarioRepository.findByDNI(propietarioRequestDto.getDni());
		if(!optional.isPresent()){
			return addPropietario(propietarioRequestDto);
		}

		if(!optional.get().isActivo()){
			optional.get().setActivo(true);
		}

		return editPropietario(optional.get().getId(), propietarioRequestDto);
	}

}
