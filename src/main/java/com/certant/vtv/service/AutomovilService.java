package com.certant.vtv.service;

import com.certant.vtv.dto.AutomovilRequestDto;
import com.certant.vtv.dto.AutomovilResponseDto;
import com.certant.vtv.entity.Automovil;
import com.certant.vtv.entity.Propietario;

import java.util.List;

public interface AutomovilService {

	Automovil addAutomovil(AutomovilRequestDto automovilRequestDto);

	Automovil decidirSiSeAgrega(AutomovilRequestDto automovilRequestDto);
	
	List<AutomovilResponseDto> getAutomoviles();

	Automovil editAutomovil(Integer id, AutomovilRequestDto automovilRequestDto);

	void deleteAutomovil(Integer id);

	Automovil getAutomovilById(Integer id);

	Propietario getPropietarioVehiculo(Integer idVehiculo);

	Automovil automovilPorPatente(String patente);
}
