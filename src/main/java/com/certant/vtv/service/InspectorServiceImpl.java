package com.certant.vtv.service;

import com.certant.vtv.constants.TipoPersonas;
import com.certant.vtv.dto.InspectorRequestDto;
import com.certant.vtv.dto.InspectorResponseDto;
import com.certant.vtv.entity.Inspector;
import com.certant.vtv.entity.Persona;
import com.certant.vtv.exception.ResourceNotFoundException;
import com.certant.vtv.repository.InspectorRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class InspectorServiceImpl implements InspectorService{

    private final InspectorRepository inspectorRepository;
    private final ModelMapper modelMapper;
	public static final String INSPECTOR_LEGAJO_EXISTENTE = "Inspector con legajo %s ya existe";


	@Autowired
    public InspectorServiceImpl(InspectorRepository inspectorRepository, ModelMapper modelMapper) {
        this.inspectorRepository = inspectorRepository;
        this.modelMapper = modelMapper;
    }
    
    
	@Override
	public void addInspector(InspectorRequestDto inspectorRequestDto) {
		if(existsLegajo(inspectorRequestDto.getLegajo())){
			String errorMessage = String.format(INSPECTOR_LEGAJO_EXISTENTE, inspectorRequestDto.getLegajo());
			throw new IllegalArgumentException(errorMessage);
		}

		Persona persona = new Persona();
		persona.setNombre(inspectorRequestDto.getNombre());
		persona.setApellido(inspectorRequestDto.getApellido());
		persona.setDni(inspectorRequestDto.getDni());
		persona.setTipo(TipoPersonas.INSPECTOR);
		persona.setActivo(true);

		Inspector entity = new Inspector(
				persona.getId(),
				persona.getNombre(),
				persona.getApellido(),
				persona.getDni(),
				inspectorRequestDto.getLegajo(),
				persona.isActivo()
		);

		inspectorRepository.save(entity);
	}

	@Override
	public List<InspectorResponseDto> getInspectores(){
		List<Inspector> allInspector = inspectorRepository.findAll();
		List<Inspector> allActivos = new ArrayList<>();

		for (Inspector inspector : allInspector) {
			if(inspector.isActivo()){
				allActivos.add(inspector);
			}
		}

		return allActivos.stream()
				.parallel()
				.map(inspector -> modelMapper.map(inspector, InspectorResponseDto.class))
				.collect(Collectors.toList());
	}

	@Override
	public void deleteInspector(Integer id) {
		Inspector inspector = getInspectorById(id);
		if(!inspector.isActivo()){
			throw new IllegalArgumentException("El inspector ya se encuentra inactivo");
		}

		inspector.setActivo(false);
		inspectorRepository.save(inspector);
	}

	@Override
	public void editInspector(Integer id, InspectorRequestDto inspectorRequestDto) {
		if(existsLegajo(id, inspectorRequestDto.getLegajo())){
			String errorMessage = String.format(INSPECTOR_LEGAJO_EXISTENTE, inspectorRequestDto.getLegajo());
			throw new IllegalArgumentException(errorMessage);
		}

		Inspector inspector = getInspectorById(id);
		inspector.setNombre(inspectorRequestDto.getNombre());
		inspector.setApellido(inspectorRequestDto.getApellido());
		inspector.setDni(inspectorRequestDto.getDni());
		inspector.setLegajo(inspectorRequestDto.getLegajo());

		inspectorRepository.save(inspector);
	}

	@Override
	public Inspector getInspectorById(Integer id) {
		Optional<Integer> optionalId = Optional.ofNullable(id);
		if (!optionalId.isPresent()) {
			throw new IllegalStateException("Id del inspector es null");
		}
		Optional<Inspector> optional = inspectorRepository.findById(id);
		if(!optional.isPresent()){
			throw  new ResourceNotFoundException("No se encontro al inspector");
		}
		return optional.get();
	}

	@Override
	public InspectorResponseDto getInspectorByIdResponse(Integer id){
    	Inspector inspector = getInspectorById(id);

    	return modelMapper.map(inspector, InspectorResponseDto.class);
	}

	@Override
	public boolean existsLegajo(String legajo) {
    	List<Inspector> list = inspectorRepository.findAll();

		for (Inspector inspector:list) {
			if(inspector.getLegajo().equals(legajo)){
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean existsLegajo(Integer id, String legajo) {
		List<Inspector> list = inspectorRepository.findAll();

		for (Inspector inspector:list) {
			if(inspector.getLegajo().equals(legajo) && inspector.getId() != id){
				return true;
			}
		}
		return false;
	}

}
