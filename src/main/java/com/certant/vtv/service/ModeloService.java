package com.certant.vtv.service;

import com.certant.vtv.dto.ModeloRequestDto;
import com.certant.vtv.dto.ModeloResponseDto;
import com.certant.vtv.entity.Modelo;

import java.util.List;

public interface ModeloService {

	void addModelo(ModeloRequestDto modeloRequestDto);
	
	List<ModeloResponseDto> getModeloByIdMarca(Integer marcaID);

	void deleteModelo(Integer id);

	void editModelo(Integer id, ModeloRequestDto modeloRequestDto);

	Modelo getModeloById(Integer id);
}
