package com.certant.vtv.service;

import com.certant.vtv.dto.VersionRequestDto;
import com.certant.vtv.dto.VersionResponseDto;
import com.certant.vtv.entity.Modelo;
import com.certant.vtv.entity.Version;
import com.certant.vtv.repository.VersionRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class VersionServiceImpl implements VersionService{


    private final VersionRepository versionRepository;
    private final ModelMapper modelMapper;
    private final ModeloService modeloService;

    @Autowired
    public VersionServiceImpl(VersionRepository versionRepository,
							  ModelMapper modelMapper,
							  ModeloService modeloService) {
        this.versionRepository = versionRepository;
        this.modelMapper = modelMapper;
        this.modeloService = modeloService;
    }


	@Override
	public void addVersion(VersionRequestDto versionRequestDto) {
		Version version = new Version();
		version.setDescripcion(versionRequestDto.getDescripcion());
		version.setModelo(modeloService.getModeloById(versionRequestDto.getModeloId()));

		versionRepository.save(version);
	}

	@Override
	public List<VersionResponseDto> getVersionesByIdModelo(Integer modelID) {
		List<Version> allVersionesPorModelo = versionRepository.findByModeloId(modelID);

		return allVersionesPorModelo.stream()
				.parallel()
				.map(version -> modelMapper.map(version, VersionResponseDto.class))
				.collect(Collectors.toList());
	}

	@Override
	public Version getVersionById(Integer id){
		Optional<Version> optional = versionRepository.findById(id);

		if(!optional.isPresent()){
			throw  new RuntimeException("La version no esta presente");
		}

		return optional.get();
	}

	@Override
	public void editVersion(Integer id, VersionRequestDto versionRequestDto) {
		Modelo modelo = modeloService.getModeloById(versionRequestDto.getModeloId());
		Version version = getVersionById(id);
		version.setModelo(modelo);
		version.setDescripcion(versionRequestDto.getDescripcion());

		versionRepository.save(version);
	}

}
