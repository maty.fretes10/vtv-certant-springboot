package com.certant.vtv.service;

import com.certant.vtv.dto.MarcaRequestDto;
import com.certant.vtv.dto.MarcaResponseDto;
import com.certant.vtv.entity.Marca;

import java.util.List;

public interface MarcaService {
	void addMarca(MarcaRequestDto marcaRequestDto);

	List<MarcaResponseDto> getMarcas();

	void deleteMarca(Integer id);

	void editMarca(Integer id, MarcaRequestDto marcaRequestDto);

	Marca getMarcaById(Integer id);
}
