package com.certant.vtv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Vtv1Application {

	public static void main(String[] args) {
		SpringApplication.run(Vtv1Application.class, args);
	}

}
