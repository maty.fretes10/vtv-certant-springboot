package com.certant.vtv.entity;

import com.certant.vtv.constants.TipoVehiculos;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "vehiculo")
@Inheritance( strategy = InheritanceType.JOINED )
public class Vehiculo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(unique = true)
    private String patente;
    private LocalDate vto;
    @Enumerated(EnumType.STRING)
    private TipoVehiculos tipo;
    private String estado;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_version", referencedColumnName = "id")
    private Version version;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_persona", referencedColumnName = "id")
    private Persona persona;

    private boolean activo;
}
