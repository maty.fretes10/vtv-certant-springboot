package com.certant.vtv.entity;

import com.certant.vtv.constants.Estado;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "inspeccion")
@Inheritance( strategy = InheritanceType.JOINED )
public class Inspeccion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private LocalDate fecha;

    @Enumerated(EnumType.STRING)
    @Column(name = "estado_observaciones")
    private Estado estadoObservaciones;

    @Enumerated(EnumType.STRING)
    @Column(name = "estado_mediciones")
    private Estado estadoMediciones;

    @Enumerated(EnumType.STRING)
    @Column(name = "estado_final")
    private Estado estadoFinal;
    private boolean exento;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_vehiculo", referencedColumnName = "id")
    private Vehiculo vehiculo;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_persona", referencedColumnName = "id")
    private Persona persona;

    private boolean activo;
}
