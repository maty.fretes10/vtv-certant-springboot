package com.certant.vtv.entity;

import lombok.*;
import javax.persistence.*;

import com.certant.vtv.constants.TipoPersonas;

@Getter
@Setter
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "persona")
@Inheritance( strategy = InheritanceType.JOINED )
public class Persona {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String nombre;
    private String apellido;
    @Column(unique = true)
    private String dni;
    @Enumerated(EnumType.STRING)
    private TipoPersonas tipo;

    private boolean activo;

}
