package com.certant.vtv.entity;

import com.certant.vtv.constants.TipoVehiculos;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "automovil")
@PrimaryKeyJoinColumn(name = "vehiculo_id")
public class Automovil extends Vehiculo{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    public Automovil(Integer id, String patente, LocalDate vto, String estado, Version version, Persona persona, boolean activo) {
        super(id, patente, vto, TipoVehiculos.AUTOMOVIL, estado, version, persona, activo);
    }
}
