package com.certant.vtv.entity;

import com.certant.vtv.constants.TipoPersonas;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "inspector")
@PrimaryKeyJoinColumn(name = "persona_id")
public class Inspector extends Persona{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "ins_legajo")
    String legajo;

    public Inspector(Integer id, String nombre, String apellido, String dni, String legajo, boolean activo) {
        super(id, nombre, apellido, dni, TipoPersonas.INSPECTOR, activo);
        setId(id);
        setLegajo(legajo);
    }
}
