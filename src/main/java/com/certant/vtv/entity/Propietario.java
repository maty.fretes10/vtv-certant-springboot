package com.certant.vtv.entity;

import com.certant.vtv.constants.TipoPersonas;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "propietario")
@PrimaryKeyJoinColumn(name = "persona_id")
public class Propietario extends Persona{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    String correo;
    String telefono;

    public Propietario(Integer id, String nombre, String apellido, String dni, String correo, String telefono, boolean activo) {
        super(id, nombre, apellido, dni, TipoPersonas.PROPIETARIO, activo);
        setCorreo(correo);
        setTelefono(telefono);
    }
}
